<?php

/**
 * @file
 * A Facet API Adapter based base path provider.
 */

/**
 * Facet API Adapter based FacetApiPrettyPathsBasePathProvider.
 */
class FacetApiPrettyPathsPanelsBasePathProvider implements FacetApiPrettyPathsBasePathProvider {

  /**
   * @param FacetapiUrlProcessorPrettyPaths $urlProcessor
   * @return base path.
   */
  public function getBasePath(FacetapiUrlProcessorPrettyPaths $urlProcessor) {
    return drupal_get_path_alias($urlProcessor->getPathWithoutSegments());
  }

}
